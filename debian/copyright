Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ktextaddons

Files: *
Copyright: 2012-2023, Laurent Montel <montel@kde.org>
License: GPL-2+

Files: debian/*
Copyright: 2023, José Manuel Santamaría Lema <panfaust@gmail.com>
           2023, Patrick Franz <deltaone@debian.org>
License: LGPL-2+

Files: textaddonswidgets/*
       textautocorrection/core/textautocorrectioncore_private_export.h
       textedittexttospeech/*
       textemoticons/*
       textgrammarcheck/textgrammarcheckprivate_export.h
       texttranslator/texttranslator_private_export.h
       texttranslator/translator/widgets/autotests/*
       texttranslator/translator/widgets/translatorconfigurecombowidget.cpp
       texttranslator/translator/widgets/translatorconfigurecombowidget.h
       texttranslator/translator/widgets/translatorconfigurelanguagelistwidget.cpp
       texttranslator/translator/widgets/translatorconfigurelanguagelistwidget.h
       texttranslator/translator/widgets/translatorconfigurelistswidget.cpp
       texttranslator/translator/widgets/translatorconfigurelistswidget.h
       texttranslator/translator/widgets/translatormenu.cpp
       texttranslator/translator/widgets/translatormenu.h
       textutils/autotests/converttexttest.cpp
       textutils/autotests/converttexttest.h
       textutils/converttext.cpp
       textutils/converttext.h
Copyright: 2020, David Faure <faure@kde.org>
           2012-2023, Laurent Montel <montel@kde.org>
License: LGPL-2+

Files: CMakeLists.txt
       CMakePresets.json.license
       README.md.license
       textaddonswidgets/CMakeLists.txt
       textaddonswidgets/KFTextAddonsWidgetsConfig.cmake.in
       textaddonswidgets/Messages.sh
       textautocorrection/CMakeLists.txt
       textautocorrection/Messages.sh
       textautocorrection/core/CMakeLists.txt
       textautocorrection/core/KFTextAutoCorrectionCoreConfig.cmake.in
       textautocorrection/core/autotests/CMakeLists.txt
       textautocorrection/widgets/CMakeLists.txt
       textautocorrection/widgets/KFTextAutoCorrectionWidgetsConfig.cmake.in
       textautocorrection/widgets/autotests/CMakeLists.txt
       textautocorrection/widgets/tests/CMakeLists.txt
       textedittexttospeech/CMakeLists.txt
       textedittexttospeech/KFTextEditTextToSpeechConfig.cmake.in
       textedittexttospeech/Messages.sh
       textedittexttospeech/autotests/CMakeLists.txt
       textedittexttospeech/tests/CMakeLists.txt
       textemoticons/CMakeLists.txt
       textemoticons/Messages.sh
       textemoticons/core/CMakeLists.txt
       textemoticons/core/KFTextEmoticonsCoreConfig.cmake.in
       textemoticons/core/autotests/CMakeLists.txt
       textemoticons/widgets/CMakeLists.txt
       textemoticons/widgets/KFTextEmoticonsWidgetsConfig.cmake.in
       textemoticons/widgets/autotests/CMakeLists.txt
       textemoticons/widgets/tests/CMakeLists.txt
       textgrammarcheck/CMakeLists.txt
       textgrammarcheck/KFTextGrammarCheckConfig.cmake.in
       textgrammarcheck/Messages.sh
       textgrammarcheck/common/CMakeLists.txt
       textgrammarcheck/common/autotests/CMakeLists.txt
       textgrammarcheck/grammalecte/autotests/CMakeLists.txt
       textgrammarcheck/grammalecte/tests/CMakeLists.txt
       textgrammarcheck/languagetool/autotests/CMakeLists.txt
       textgrammarcheck/languagetool/tests/CMakeLists.txt
       texttranslator/CMakeLists.txt
       texttranslator/KFTextTranslatorConfig.cmake.in
       texttranslator/Messages.sh
       texttranslator/designer/*
       texttranslator/translator/autotests/CMakeLists.txt
       texttranslator/translator/plugins/CMakeLists.txt
       texttranslator/translator/plugins/bing/CMakeLists.txt
       texttranslator/translator/plugins/bing/autotests/CMakeLists.txt
       texttranslator/translator/plugins/deepl/CMakeLists.txt
       texttranslator/translator/plugins/deepl/autotests/CMakeLists.txt
       texttranslator/translator/plugins/google/CMakeLists.txt
       texttranslator/translator/plugins/google/autotests/CMakeLists.txt
       texttranslator/translator/plugins/libretranslate/CMakeLists.txt
       texttranslator/translator/plugins/libretranslate/autotests/CMakeLists.txt
       texttranslator/translator/plugins/lingva/CMakeLists.txt
       texttranslator/translator/plugins/lingva/autotests/CMakeLists.txt
       texttranslator/translator/plugins/yandex/CMakeLists.txt
       texttranslator/translator/plugins/yandex/autotests/CMakeLists.txt
       texttranslator/translator/tests/CMakeLists.txt
       texttranslator/translator/widgets/autotests/CMakeLists.txt
       textutils/CMakeLists.txt
Copyright: 2019-2023, Laurent Montel <montel@kde.org>
License: BSD-3-Clause

Files: .gitlab-ci.yml
       .kde-ci.yml
       .reuse/dep5
       sanitizers.supp
       textemoticons/core/emoji.qrc
       textemoticons/widgets/tests/test.qrc
       textutils/autotests/CMakeLists.txt
       textutils/KFTextUtilsConfig.cmake.in
Copyright: 2022-2023, Laurent Montel <montel@kde.org>
License: CC0-1.0

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA. Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2’.

License: LGPL-2+
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option)  any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in
 `/usr/share/common-licenses/LGPL-2’.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all
 copyright and related and neighboring rights to this software to the
 public domain worldwide. This software is distributed without any
 warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication
 along with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 Public Domain
 Dedication can be found in `/usr/share/common-licenses/CC0-1.0'.
